#include "../../include/functions/rotate.h"
#include <stdio.h>

static uint64_t pixel_address(const bool input, const uint64_t width, const uint64_t i, const uint64_t j) {
    if (input) {
        return j * width + i;
    } else return (i + 1) * width - j - 1;
}

void rotate(struct image const source, struct image * out) {
    fprintf(stderr, "%s", "Rotate...\n");
    const uint64_t out_height = source.width;
    const uint64_t out_width = source.height;

    image_create(out_width, out_height, out);
    for (uint64_t i = 0; i < out_height; i++) {
        for (uint64_t j = 0; j + 1 <= out_width; j++) {

                out->data[pixel_address(false, out_width, i, j)] = source.data[pixel_address(true, out_height, i, j)];

        }
    }
}
