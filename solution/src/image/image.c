#include "../../include/image/image.h"
#include <stdlib.h>

void image_create(const uint32_t width, const uint32_t height, struct image * img) {
    img->width = width;
    img->height = height;
    img->data = malloc(image_size(img));
}

void image_delete(const struct image * img) {
    free(img->data);
}

uint32_t image_size(const struct image * img) {
    return img->height * img->width * sizeof(struct pixel);
}
