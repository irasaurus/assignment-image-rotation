#include "../../../../include/file/file_formats/bmp/bmp_handler.h"

#define MAGIC_VALUE         19778
#define NUM_PLANE           1

struct __attribute__((packed)) bmp_header
{
    uint16_t bf_type;
    uint32_t  bfile_size;
    uint32_t bf_reserved;
    uint32_t b_off_bits;
    uint32_t bi_size;
    uint32_t bi_width;
    uint32_t  bi_height;
    uint16_t  bi_planes;
    uint16_t bi_bit_count;
    uint32_t bi_compression;
    uint32_t bi_size_image;
    uint32_t bi_PPM_x;
    uint32_t bi_PPM_Y;
    uint32_t bi_clr_used;
    uint32_t  bi_clr_important;
};

static enum returning_errors read_header( FILE* const in, struct bmp_header * const header );
static enum returning_errors read_bmp(FILE* const file, const struct image* image );
static enum returning_errors write_bmp(FILE* const file, const struct image* image );
static struct bmp_header create_bmp_header( const struct image* img );
static uint32_t get_padding( const uint32_t width );

enum returning_errors from_bmp( FILE* const in, struct image* img ) {
    struct bmp_header header = {0};
    enum returning_errors file_status = read_header(in, &header);
    if (file_status != SUCCESS) return file_status;
    image_create(header.bi_width, header.bi_height, img);
    if (fseek(in, header.b_off_bits, SEEK_SET) != 0) return BMP_ERROR;

    return read_bmp(in, img);
}

enum returning_errors to_bmp( FILE* const out, struct image const * img ) {
    const struct bmp_header header = create_bmp_header(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) return FILE_WRITE_ERROR;
    return write_bmp(out, img);
}

uint32_t get_padding( const uint32_t width ) {
    const uint32_t pad = width % 4;
    return pad;
}

static enum returning_errors read_header( FILE* const in, struct bmp_header * const header ) {
    if (fread(header, sizeof (struct bmp_header), 1, in) == 1) return SUCCESS;
    return HEADER_ERROR;
}
static enum returning_errors read_bmp(FILE* const file, const struct image* image ) {
    const uint8_t pad = get_padding(image->width);
    for (uint32_t i = 0; i < image->height; i++) {
        for (uint32_t j = 0; j < image->width; j++) {
            if (!fread( image->data + image->width * i + j, sizeof (struct pixel), 1, file)) return BMP_READ_ERROR;
        }
        if (fseek(file, pad, SEEK_CUR)  != 0 ) return PADDING_SEEK_ERROR;
    }
    return SUCCESS;
}
static enum returning_errors write_bmp(FILE* const file, const struct image* image ) {
    const uint8_t pad = get_padding(image->width);
    const uint8_t paddings[3] = {0};

    for (uint32_t i = 0; i < image->height ; i++) {
        if (!fwrite(image->width * i + image->data, sizeof (struct pixel) * image->width, 1, file)) return BMP_WRITE_ERROR;
        if (!fwrite(paddings, pad, 1, file)) return PADDING_WRITE_ERROR;
    }

    return SUCCESS;
}
static struct bmp_header create_bmp_header(const struct image * img) {
    const struct bmp_header bmp = {
            .bf_type = MAGIC_VALUE,
            .bfile_size = sizeof(struct bmp_header) + image_size(img),
            .bf_reserved = 0,
            .b_off_bits = sizeof(struct bmp_header),
            .bi_size = 40,
            .bi_width = img->width,
            .bi_height = img->height,
            .bi_planes = NUM_PLANE,
            .bi_bit_count = 24,
            .bi_compression = 0,
            .bi_size_image = image_size(img),
            .bi_PPM_x = 2834,
            .bi_PPM_Y = 2834,
            .bi_clr_used = 0,
            .bi_clr_important = 0
    };
    return bmp;
}
