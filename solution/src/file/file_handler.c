#include "../../include/file/file_handler.h"
#include "../../include/file/file_formats/bmp/bmp_handler.h"
#include <errno.h>

static enum returning_errors close_file( FILE* file ) {
    if (fclose(file) == EOF) return FILE_CLOSE_ERROR;
    return SUCCESS;
}

enum returning_errors read_file( const char* const directory, struct image * const img ) {
    FILE* const file = fopen(directory, "rb");
    if (errno != 0) return FILE_READ_ERROR;
    enum returning_errors img_read = from_bmp(file, img);
    if (img_read != SUCCESS) return img_read;
    return close_file(file);
}

enum returning_errors write_file( const char* directory, struct image * const img ) {
    FILE* file;
    if ((file = fopen(directory, "wb")) == NULL) return FILE_WRITE_ERROR;
    enum returning_errors img_write = to_bmp(file, img);
    if (img_write != SUCCESS) return img_write;
    return close_file(file);
}
