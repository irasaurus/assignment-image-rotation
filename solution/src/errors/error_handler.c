#include "../../include/errors/error_handler.h"
#include <stdio.h>

static const char* const returning_errors_message[] = {
        [SUCCESS] = "Success.",
        [FILE_READ_ERROR] = "Cannot read a file, please recheck file path or permissions.",
        [FILE_WRITE_ERROR] = "Cannot write a file, please check dependency settings or permissions.",
        [FILE_CLOSE_ERROR] = "Unable to close a file.",
        [BMP_ERROR] = "Error occurred with bmp file.",
        [HEADER_ERROR] = "Error occurred during reading bmp header.",
        [BMP_READ_ERROR] = "Cannot read bmp file.",
        [BMP_WRITE_ERROR] = "Cannot write bmp file.",
        [PADDING_SEEK_ERROR] = "Error occurred while adding padding (read function).",
        [PADDING_WRITE_ERROR] = "Error occurred while adding padding (write function)."
};

void print_error(const char* const message) {
    fprintf(stderr, "%s", message);
    fprintf(stderr, "%s", "\n");
}

void returning_errors_print(const char* const process, const enum returning_errors error) {
    print_error(process);
    print_error(returning_errors_message[error]);
}
