#include "file/file_handler.h"
#include "file/file_formats/bmp/bmp_handler.h"
#include "functions/rotate.h"
#include "image/image.h"

int main( int argc, char** argv ) {
    if (argc != 3)
    {
        fprintf(stderr, "%s", "Wrong args.\nFormat: ./build/solution/image-transformer <source-image> <transformed-image>\n");
        return 0;
    }

    const char* const input_file = argv[1];
    const char* const output_file = argv[2];
    struct image input_image = {0};
    struct image output_image = {0};

    const enum returning_errors read_file_status = read_file(input_file, &input_image);
    returning_errors_print("Reading file...", read_file_status);
    if (read_file_status != SUCCESS) {
        image_delete(&input_image);
        return 0;
    }

    rotate(input_image, &output_image);
    const enum returning_errors write_file_status = write_file(output_file, &output_image);
    returning_errors_print("Writing file...", write_file_status);
    if (write_file_status != SUCCESS) {
        image_delete(&input_image);
        image_delete(&output_image);
        return 0;
    }

    image_delete(&input_image);
    image_delete(&output_image);
    return 0;
}
