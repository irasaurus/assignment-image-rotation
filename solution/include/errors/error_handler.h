#ifndef MY_ASSIGNMENT_IMAGE_ROTATION_ERROR_HANDLER_H
#define MY_ASSIGNMENT_IMAGE_ROTATION_ERROR_HANDLER_H
enum returning_errors {
    SUCCESS,
    FILE_READ_ERROR,
    FILE_WRITE_ERROR,
    FILE_CLOSE_ERROR,
    BMP_ERROR,
    HEADER_ERROR,
    BMP_READ_ERROR,
    BMP_WRITE_ERROR,
    PADDING_SEEK_ERROR,
    PADDING_WRITE_ERROR
};

void returning_errors_print(const char* const process, const enum returning_errors error);
#endif //MY_ASSIGNMENT_IMAGE_ROTATION_ERROR_HANDLER_H
