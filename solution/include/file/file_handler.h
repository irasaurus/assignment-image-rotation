#ifndef MY_ASSIGNMENT_IMAGE_ROTATION_FILE_HANDLER_H
#define MY_ASSIGNMENT_IMAGE_ROTATION_FILE_HANDLER_H

#include "../errors/error_handler.h"
#include "../image/image.h"
#include <stdio.h>

enum returning_errors read_file( const char* directory, struct image * const img );
enum returning_errors write_file( const char* directory, struct image * const img );
#endif //MY_ASSIGNMENT_IMAGE_ROTATION_FILE_HANDLER_H
