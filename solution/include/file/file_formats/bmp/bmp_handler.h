#ifndef MY_ASSIGNMENT_IMAGE_ROTATION_BMP_HANDLER_H
#define MY_ASSIGNMENT_IMAGE_ROTATION_BMP_HANDLER_H

#include "../../../errors/error_handler.h"
#include "../../../image/image.h"
#include <stdio.h>
#include <stdlib.h>

enum returning_errors from_bmp( FILE* const in, struct image* img );
enum returning_errors to_bmp( FILE* const out, struct image const* img );
#endif //MY_ASSIGNMENT_IMAGE_ROTATION_BMP_HANDLER_H
