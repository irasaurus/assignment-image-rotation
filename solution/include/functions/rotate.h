#ifndef MY_ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define MY_ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#include "../image/image.h"
#include <stdbool.h>
void rotate(struct image const source, struct image * out);
#endif //MY_ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
