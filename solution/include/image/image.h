#ifndef MY_ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define MY_ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#include <stdint.h>
#include <stdlib.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint32_t width, height;
    struct pixel* data;
};

void image_create(const uint32_t width, const uint32_t height, struct image * img);
void image_delete(const struct image * img);
uint32_t image_size(const struct image * img);

#endif //MY_ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
